const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const DIST_DIR   = path.resolve(__dirname, "dist");
const CLIENT_DIR = path.resolve(__dirname, "src");

module.exports = {
    entry: './src/index.js',
    output: {
        filename: '[name].js',
        publicPath: "/",
        path: DIST_DIR
    },
    module: {
        rules: [
        {
            test: /\.styl$/,
            use: [
                {
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        hmr: process.env.NODE_ENV === 'development',
                        },
                
                },
                { loader: 'css-loader'},
                { loader: 'stylus-loader'},
            ]
        },
        {
            test: /\.css$/,
            use: [
                {
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        hmr: process.env.NODE_ENV === 'development',
                        },
                
                },
                { loader: 'css-loader'},
            ]
        },        
        {
            test: /\.vue$/,
            use: [
                { loader: 'vue-loader' }
            ]
        }
      ]
    },
    resolve: {
        alias: {
          'vue$': 'vue/dist/vue.esm.js',
          'commonStyles': CLIENT_DIR + '/common.styl',
          'modules': CLIENT_DIR + '/modules/',

        }
    },
    plugins:[
        new HtmlWebpackPlugin({
            title: 'test',
            template: './src/index.html',
            inject: false
        }),
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: 'styles.css',
          }),
    ],
    node: {fs: 'empty'}
  }
