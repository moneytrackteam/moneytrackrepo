require("./index.styl")

if (module.hot) {
  module.hot.accept();
 }

import Vue from 'vue';
import headerTemplate from 'modules/headerComponent.vue'
import mainPanelTemplate from 'modules/mainPanel.vue'

import Vuex from 'vuex';
import {mapState} from 'vuex';
import {mapActions} from 'vuex';


import jsonLoader from 'modules/jsonLoader'

var app;
var store;
var methods = {};
var data = {};


setVuex();
setVue();


function setVuex(){
  Vue.use(Vuex);
  store = new Vuex.Store({
    state: {
      isBalanceVisible: true,
      isPaiementsVisible: false,
      balanceValue: 0,
      articles: [],
      paiements: [],
    },
    mutations:{
      showBalance(state){
        state.isPaiementsVisible = false;
        state.isBalanceVisible = true;
      },
      showPaiements(state){
        state.isBalanceVisible = false;
        state.isPaiementsVisible = true;
      },
      balanceLoaded(state,data){
        state.balanceValue = data.balance;
      },   
      articlesLoaded(state,data){
        state.articles = data
      },   
      paiementsLoaded(state,data){
        state.paiements = data
      },   
    },      
    actions:{
      loadBalance({ commit }){
          jsonLoader('http://private-70cd8-testtechniquefront.apiary-mock.com/balance',function(data){
            commit('balanceLoaded',data)
          })
      },
      loadPaiements({ commit }){
        jsonLoader('https://private-70cd8-testtechniquefront.apiary-mock.com/articles',function(data){
            commit('articlesLoaded',data)
            jsonLoader('https://private-70cd8-testtechniquefront.apiary-mock.com/payments',function(data){
              commit('paiementsLoaded',data)
            })
        })        

      },
  },    
    modules:{}
  })
}



function setVue(){
    app = new Vue({
    el: '#app',
    data:data,
    store,
    methods: methods,
    computed: mapState([]),
    components: {
      'header-component':headerTemplate,
      'main-panel':mainPanelTemplate,
      }
  });
}
  