var path = require("path");
var express = require("express");

const app = express();


var webpack = require("webpack");
var webpackDevMiddleware = require("webpack-dev-middleware");
var webpackHotMiddleware = require("webpack-hot-middleware");
var devConfig = require("./webpack.dev.config.js");
var prodConfig = require("./webpack.prod.config.js");

const DIST_DIR = path.join(__dirname, "dist");
const HTML_FILE     = path.join(DIST_DIR, "index.html");

const isDevelopment = process.env.NODE_ENV === "development";

console.log("isDevelopment :" + isDevelopment)
// Tell express to use the webpack-dev-middleware and use the webpack.config.js
// configuration file as a base.

if(isDevelopment){
    
    var compiler = webpack(devConfig);
    app.use("/",webpackDevMiddleware(compiler, {
        publicPath: devConfig.output.publicPath
    }));
    app.use("/",webpackHotMiddleware(compiler));
    
}

else{
     app.use(express.static(DIST_DIR));
	app.get("*", (req, res) => res.sendFile(HTML_FILE));
}

// error handler
app.use(function(err,req,res,next){
    res.locals.message = err.message;
    res.locals.error = err;
    console.log("ERROR => ", res.locals.message)
    res.status(500);
    res.json({'error':res.locals.message});
});

// Serve the files on port 3001.
app.listen(3001, function () {
    console.log('listening on port 3001!\n');
});